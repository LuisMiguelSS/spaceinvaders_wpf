﻿using SpaceInvaders.assets.modelos;
using SpaceInvaders.assets.utils;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SpaceInvaders.assets.utils.GameEngine;

namespace SpaceInvaders
{

    public partial class MainWindow : Window
    {
        //
        // Atributos
        //
        private readonly GameEngine Engine;
        private int score = 0;
        private int ShipInUse = 0;
        private Player player;

        //
        // Constructor
        //
        public MainWindow()
        {
            InitializeComponent();

            Engine = new GameEngine(canvasPrincipal);
            player = new Player(this, canvasPrincipal);
        }

        private void BtnNaveEspacial_Click(object sender, RoutedEventArgs e)
        {
            ShipInUse = int.Parse(Regex.Match((sender as Button).Tag.ToString(), @"\d+").Value);

            Storyboard animacion = ((Storyboard)FindResource("animationFadeOut")).Clone(); // Clonamos para evitar congelación (ver Storyboard Animation IsFrozen)
            animacion.Completed += (send, args) =>
            {
                stackPanelMenu.Visibility = Visibility.Collapsed;
                StartGame();
            };

            animacion.Begin(stackPanelMenu);
        }

        //
        // Getters
        //
        public BitmapImage GetPlayerShipImage()
        {
            return new BitmapImage(new Uri(@"pack://application:,,,/SpaceInvaders;component/src/assets/spaceships/ship" + ShipInUse + "/Ship" + ShipInUse + ".png"));
        }
        public BitmapImage GetEnemyImage()
        {
            return new BitmapImage(new Uri(@"pack://application:,,,/SpaceInvaders;component/src/assets/enemies/alien.gif"));
        }


        //
        // Otros métodos
        //
        private void StartGame()
        {
            labelScore.Content = "Puntos: 0";

            // Get all new
            Engine.ReloadProperties();

            // Update Player skin
            player.SetDamage(ShipInUse);
            player.SetImage(new ImageBrush(GetPlayerShipImage()));

            // Set shot speed
            switch (ShipInUse)
            {
                case 3:
                    player.SetShotSteps(ShotSteps.VeryFast);
                    break;
                case 2:
                    player.SetShotSteps(ShotSteps.Fast);
                    break;
                case 4:
                    player.SetShotSteps(ShotSteps.Normal);
                    break;
                case 1:
                    player.SetShotSteps(ShotSteps.VerySlow);
                    break;
            }

            // Set shot damage
            switch (player.GetShotSteps())
            {
                case ShotSteps.VeryFast:
                    player.SetDamage(0.28);
                    break;
                case ShotSteps.Fast:
                    player.SetDamage(0.5);
                    break;
                case ShotSteps.Normal:
                    player.SetDamage(1);
                    break;
                case ShotSteps.Slow:
                    player.SetDamage(1.7);
                    break;
                case ShotSteps.VerySlow:
                    player.SetDamage(2.1);
                    break;
            }

            Engine.AddToCanvas(player, Engine.GetPlayerStartingPosition().X, Engine.GetPlayerStartingPosition().Y);

            // Add enemies
            Engine.PositionEnemies();
        }
        public void updateScore()
        {
            score++;
            labelScore.Content = "Puntos: " + score;

            // End game
            if (score == MAX_ENEMIES)
                RestartGame();
        }

        private void RestartGame()
        {
            score = 0;

            // Copy enemies to list for deletion
            List<UIElement> rectangles = new List<UIElement>();

            foreach(UIElement uiel in canvasPrincipal.Children)
                if (uiel is Rectangle)
                    rectangles.Add(uiel);

            foreach (UIElement el in rectangles)
                canvasPrincipal.Children.Remove(el);

            // Fade back the menu
            Storyboard animacion = ((Storyboard)FindResource("animationFadeIn")).Clone();
            animacion.Completed += (send, args) => stackPanelMenu.Visibility = Visibility.Visible;

            animacion.Begin(stackPanelMenu);
        }

        //
        // Listeners
        //

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (ShipInUse == 0)
                return;

            if (e.IsRepeat || e.IsDown)
            {
                switch (e.Key.ToString())
                {
                    // Movement
                    case "Left":
                    case "A":
                    case "a":
                        Engine.MovePlayer(player, Movement.Left);
                        break;
                    case "Right":
                    case "D":
                    case "d":
                        Engine.MovePlayer(player, Movement.Right);
                        break;

                    // Shoot
                    case "Space":
                        player.Shoot();
                        break;

                    // Restart Game
                    case "Escape":
                        RestartGame();
                        break;
                }
            }
        }
    }
}
