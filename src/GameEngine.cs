﻿using SpaceInvaders.assets.modelos;
using SpaceInvaders.src.models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SpaceInvaders.assets.utils
{
    public class GameEngine
    {
        // Movement
        public enum Movement
        {
            Left, Right
        }
        public enum ShotSteps
        {
            VeryFast = 11,
            Fast = 9,
            Normal = 5,
            Slow = 4,
            VerySlow = 2
        }

        //
        // Atributos
        //
        private Canvas canvas;
        public const int MAX_ENEMIES = 39;
        private double MovementSteps = 20;
        public const double ShotFPS = 16.67;
        private int enemigosColocadosEnPantalla = 0;
        private double LastEnemyXPosition = 0;

        // Enemigos
        private static double EnemySizeSeparation = 30;

        // Jugador
        private readonly double ReservedPlayerSpaceHeight = 95;
        private readonly double SecurePlayerSpace = EnemySizeSeparation - 20 > 0 ? EnemySizeSeparation - 20 : EnemySizeSeparation;

        private double PlayerStarting_X;
        private double PlayerStarting_Y;


        //
        // Constructor
        //
        public GameEngine(Canvas canvas)
        {
            this.canvas = canvas;

            ReloadProperties();
        }

        //
        // Getters
        //
        public Canvas GetCanvas() { return canvas; }
        public double GetCanvasXCenter() { return canvas.ActualWidth / 2; }
        public double GetCanvasYCenter() { return canvas.ActualHeight / 2; }
        public double GetReservedPlayerAreaHeight() { return ReservedPlayerSpaceHeight; }
        public Point GetPlayerStartingPosition() { return new Point(PlayerStarting_X, PlayerStarting_Y); }
        public double GetEnemySideSeparation() { return EnemySizeSeparation; }

        //
        // Otros métodos
        //
        public void ReloadProperties()
        {
            PlayerStarting_X = GetCanvasXCenter();
            PlayerStarting_Y = canvas.ActualHeight - ReservedPlayerSpaceHeight + Player.SIZE;
        }

        // Add To Canvas
        public void AddToCanvas(CharacterInterface character, Point position)
        {
            AddToCanvas(character, position.X, position.Y);
        }
        public void AddToCanvas(CharacterInterface character, double x, double y)
        {
            canvas.Children.Add(character.GetFigure());

            Canvas.SetLeft(character.GetFigure(), x);
            Canvas.SetTop(character.GetFigure(), y);
        }

        public void PositionEnemies()
        {
            int maxEnemiesPerRow = MAX_ENEMIES;

            // Create MAX_ENEMIES number of enemies.
            while (Enemy.SIZE * maxEnemiesPerRow + 20 * maxEnemiesPerRow >= canvas.ActualWidth - 20)
                maxEnemiesPerRow--;

            // Calculate the enemies in a row and number of rows for enemies separation.
            int enemiesInThisRow = 0;
            int numberOfRows = 0;
            for (int newEnemy = 1; newEnemy <= MAX_ENEMIES; newEnemy++)
            {
                if (enemiesInThisRow == maxEnemiesPerRow)
                {
                    enemiesInThisRow = 0;
                    numberOfRows++;
                }

                enemiesInThisRow++;

                int newX = Enemy.SIZE * enemiesInThisRow + 20 * enemiesInThisRow;
                int newY = Enemy.SIZE * numberOfRows + 20 * numberOfRows + 10;

                if (newEnemy < 11)
                    AddToCanvas(new Enemy(2), newX, newY);
                else
                    AddToCanvas(new Enemy(1), newX, newY);

            }


        }
        // Get Enemies' Coordinates
        public List<Point> GetCoordinatesForEnemies()
        {
            List<Point> posicionesEnemigos = new List<Point>();
            List<int> enemigosPorFila = new List<int>();

            for (int enemy = 1; enemy <= MAX_ENEMIES; enemy++)
            {
                if ((Enemy.SIZE +
                        GetEnemySideSeparation() * 2) * enemigosColocadosEnPantalla
                        <
                        (Enemy.SIZE + GetEnemySideSeparation() * 2)
                    )
                {

                    // New position
                    Point nuevaPos = new Point();
                    nuevaPos.X = LastEnemyXPosition + GetEnemySideSeparation() * 2;
                    nuevaPos.Y = enemigosPorFila.Count * Enemy.SIZE +
                                 GetEnemySideSeparation() * 2 * enemigosPorFila.Count;

                    // Save as new position
                    LastEnemyXPosition = nuevaPos.X;

                    if (enemigosPorFila.Count == 0)
                        enemigosPorFila.Add(1); // Add new row
                    else
                        enemigosPorFila[enemigosPorFila.Count - 1]++; // Increment enemy count in that row

                    posicionesEnemigos.Add(nuevaPos);
                    enemigosColocadosEnPantalla++;

                }
                else
                {
                    // Add new row
                    enemigosPorFila.Add(1);

                    // Set new enemy position
                    Point nuevaPos = new Point();
                    nuevaPos.X = LastEnemyXPosition + GetEnemySideSeparation() * 2;
                    nuevaPos.Y = enemigosPorFila.Count * (Enemy.SIZE + GetEnemySideSeparation() * 2);

                    // Save as latest values
                    LastEnemyXPosition = nuevaPos.X;

                    posicionesEnemigos.Add(nuevaPos);
                    enemigosColocadosEnPantalla++;
                }
            }

            return posicionesEnemigos;
        }

        // Can Move
        private bool CanMove(Player player, Movement direction)
        {
            if (player == null)
                return false;

            double playerXPos = Canvas.GetLeft(player.GetFigure());

            switch (direction)
            {
                case Movement.Left:
                    return playerXPos - SecurePlayerSpace >= 0;

                case Movement.Right:
                    return playerXPos + player.GetFigure().Width + SecurePlayerSpace <= canvas.ActualWidth;
            }

            return false;
        }

        public void MovePlayer(Player player, Movement direction)
        {
            if (!CanMove(player, direction))
                return;

            double playerXPos = Canvas.GetLeft(player.GetFigure());

            switch (direction)
            {
                case Movement.Left:
                    Canvas.SetLeft(player.GetFigure(), playerXPos - MovementSteps);
                    break;
                case Movement.Right:
                    Canvas.SetLeft(player.GetFigure(), playerXPos + MovementSteps);
                    break;
            }

        }


    }
}
