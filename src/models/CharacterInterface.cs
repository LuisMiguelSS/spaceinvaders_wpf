﻿using System.Windows.Shapes;

namespace SpaceInvaders.src.models
{
    public interface CharacterInterface
    {
        Rectangle GetFigure();
    }
}
