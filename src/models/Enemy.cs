﻿using SpaceInvaders.src.models;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SpaceInvaders.assets.modelos
{
    class Enemy : CharacterInterface
    {
        //
        // Atributos
        //
        public static int SIZE = 36;

        private double Health;
        private Rectangle figure;

        //
        // Constructor
        //
        public Enemy() : this(1)
        { }
        public Enemy(double Health) : this(
            new Uri(@"pack://application:,,,/SpaceInvaders;component/src/assets/enemies/alien.gif"),
            Health)
        { }
        public Enemy(Uri image, double health) :

            this( new ImageBrush(new BitmapImage(image)),
                  health)
        { }
        public Enemy(Brush image, double health)
        {
            figure = new Rectangle
            {
                Fill = image,
                Tag = "enemy;health=" + health,
                Width = SIZE,
                Height = SIZE,
                Visibility = Visibility.Visible
            };

            Health = health;

        }

        //
        // Getters
        //
        public Rectangle GetFigure() { return figure; }
        public double GetHealth() { return Health; }

        //
        // Setters
        //
        public void SetImage(Brush image) { figure.Fill = image; }
        public void SetHealth(double health) { Health = health; }

    }
}
