﻿using SpaceInvaders.assets.utils;
using SpaceInvaders.src.models;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using static SpaceInvaders.assets.utils.GameEngine;

namespace SpaceInvaders.assets.modelos
{
    public class Player : ObjectDataProvider, CharacterInterface
    {
        //
        // Attributes
        //

        // Figure
        public static int SIZE = 60;
        private Rectangle figure;

        // Parents
        private readonly Canvas canvas;
        private MainWindow main;

        // Game
        private double health;
        private double movementSpeed;

        // Shot
        private double shotDamage;
        private ShotSteps shotSteps;
        private int shotsFired;

        //
        // Constructor
        //
        public Player(MainWindow main, Canvas canvas) : this(
            main,
            canvas,
            new Uri(@"pack://application:,,,/SpaceInvaders;component/src/assets/spaceships/ship1/Ship1.png"),
            1,
            4)
        { }
        public Player(MainWindow main, Canvas canvas, Uri image, double shotDamage, double movementSpeed) :

            this( main,
                  canvas,
                  new ImageBrush(new BitmapImage(image)),
                  shotDamage,
                  movementSpeed)
        { }
        public Player(MainWindow main, Canvas canvas, Brush image, double shotDamage, double movementSpeed) :
            this(main, canvas, image, shotDamage, movementSpeed, ShotSteps.Normal)
        { }
        public Player(MainWindow main, Canvas canvas, Brush image, double shotDamage, double movementSpeed, ShotSteps shotSteps)
        {
            health = 5;
            this.canvas = canvas;
            this.main = main;
            figure = new Rectangle
            {
                Fill = image,
                RenderTransform = new RotateTransform(-90),
                Tag = "player;health=" + health,
                Width = SIZE,
                Height = SIZE,
                Visibility = Visibility.Visible
            };

            shotsFired = 0;
            this.shotDamage = shotDamage;
            this.movementSpeed = movementSpeed;
            this.shotSteps = shotSteps;
        }

        //
        // Getters
        //
        public Rectangle GetFigure() { return figure; }
        public ImageSource GetImage()
        {
            if (figure == null)
                return null;

            return (figure.Fill as ImageBrush).ImageSource;
        }
        public double GetDamage() { return shotDamage; }
        public double GetSpeed() { return movementSpeed; }
        public int GetShotsFired() { return shotsFired; }
        public ShotSteps GetShotSteps() { return shotSteps; }

        //
        // Setters
        //
        public void SetImage(Brush newImage) { figure.Fill = newImage; }
        public void SetImage(BitmapImage newImage) { figure.Fill = new ImageBrush(newImage); }
        public void SetDamage(double damage) { shotDamage = damage; }
        public void SetSpeed(double speed) { movementSpeed = speed; }
        public void SetShotSteps(ShotSteps steps) { this.shotSteps = steps; }

        //
        // Other methods
        //
        public void Shoot()
        {

            shotsFired++;

            Shot shot = new Shot();

            canvas.Children.Add(shot.GetShape());
            Canvas.SetLeft(shot.GetShape(), Canvas.GetLeft(figure) + figure.Width / 2 - shot.GetShape().Width / 2);
            Canvas.SetTop(shot.GetShape(), Canvas.GetTop(figure) - figure.Height);

            // Timer for FPS
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(GameEngine.ShotFPS);
            timer.Tick += (s, e) =>
            {

                if (Canvas.GetTop(shot.GetShape()) <= 0)
                {
                    // Shot hit nothing
                    timer.Stop();
                    canvas.Children.Remove(shot.GetShape());
                }

                else
                {
                    // Shot as Rect and not Rectangle so the method Intersects with can be used
                    Rect shotAsRect = new Rect(Canvas.GetLeft(shot.GetShape()), Canvas.GetTop(shot.GetShape()), shot.GetShape().Width, shot.GetShape().Height);

                    for (int i = 0; i < canvas.Children.Count; i++)
                    {
                        UIElement item = canvas.Children[i];

                        if (item is Rectangle)
                        {
                            Rectangle figureInCanvas = item as Rectangle;

                            Rect enemyAsRect = new Rect(Canvas.GetLeft(figureInCanvas), Canvas.GetTop(figureInCanvas), figureInCanvas.Width, figureInCanvas.Height);

                            if (figureInCanvas.Tag != null && figureInCanvas.Tag.ToString().Contains("enemy")) // Is an enemy
                                if (Rect.Intersect(shotAsRect, enemyAsRect) != Rect.Empty)
                                {
                                    // The shot hit an enemy
                                    canvas.Children.Remove(shot.GetShape());

                                    // Check if enemy is dead or has more health
                                    int enemyHealth = int.Parse(Regex.Match(figureInCanvas.Tag.ToString(), @"\d+").Value);

                                    if (enemyHealth - shotDamage <= 0)
                                    {

                                        main.updateScore();
                                        canvas.Children.Remove(figureInCanvas);

                                        // Explosion animation
                                        Rectangle enemyDeadImage = new Rectangle
                                        {
                                            Fill = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/SpaceInvaders;component/src/assets/green-splash.gif"))),
                                            Width = Enemy.SIZE,
                                            Height = Enemy.SIZE,
                                            Visibility = Visibility.Visible
                                        };

                                        Storyboard animacion = ((Storyboard)canvas.FindResource("explode")).Clone();
                                        animacion.Completed += (send, args) => {
                                            enemyDeadImage.Visibility = Visibility.Collapsed;
                                        };
                                        
                                        canvas.Children.Add(enemyDeadImage);
                                        Canvas.SetLeft(enemyDeadImage, Canvas.GetLeft(figureInCanvas));
                                        Canvas.SetTop(enemyDeadImage, Canvas.GetTop(figureInCanvas));

                                        animacion.Begin(enemyDeadImage);
                                        
                                    }
                                    else
                                        figureInCanvas.Tag = "enemy;health=" + (enemyHealth - shotDamage);


                                    timer.Stop();
                                }
                        }
                    }
                }

                // Move shot upwards
                Canvas.SetTop(shot.GetShape(), Canvas.GetTop(shot.GetShape()) - (double)shotSteps);

            };
            timer.Start();
        }
    }
}
