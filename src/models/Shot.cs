﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SpaceInvaders.assets.modelos
{
    class Shot
    {
        //
        // Attributes
        //
        private Rectangle shape;

        //
        // Constructors
        //
        public Shot() : this(Brushes.Red,
                            new Rectangle
                            {
                                Tag = "shot",
                                StrokeThickness = 20,
                                Width = 4,
                                Height = 7,
                                Visibility = Visibility.Visible
                            })
        { }
        public Shot(SolidColorBrush color, Rectangle shape)
        {
            this.shape = shape;
            shape.Fill = color;
        }

        //
        // Getters
        //
        public Rectangle GetShape() { return shape; }
        public Brush GetColor() { return shape.Fill; }

        //
        // Setters
        //
        public void setColor(Brush brush) { shape.Fill = brush; }
    }
}
